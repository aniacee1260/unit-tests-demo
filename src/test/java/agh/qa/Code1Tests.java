package agh.qa;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Code1Tests {

    @Test
    public void Test1(){
        Code1 obj1 = new Code1();

        Assert.assertEquals(obj1.calculate(12), 14);
    }

    @Test
    public void Test2(){
        Code1 obj1 = new Code1();

        Assert.assertEquals(obj1.calculate(1), 2);
    }
}
